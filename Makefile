.PHONY: gen
gen: clear
	docker run --rm -v $(shell pwd):/project openapitools/openapi-generator-cli:v7.1.0 \
		generate -i /project/petstore.yaml -g go \
		--additional-properties=packageName=component -o /project/gen/component -p enumClassPrefix=true

.PHONY: clear
clear:
	ls gen/component | grep -v -E '.openapi-generator-ignore|go\.mod|go\.sum' | xargs -I {} rm -r gen/component/{}
